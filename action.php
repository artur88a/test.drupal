<?php
function clean($value = "") {
    $value = trim($value);
    $value = stripslashes($value);
    $value = strip_tags($value);
    $value = htmlspecialchars($value);

    return $value;
}
if ($_POST['email']){
    $email = $_POST['email'];
    if ($_POST['name']){
        $name = $_POST['name'];
        $name = clean($name);
    }
    $email = clean($email);
    $email_validate = filter_var($email, FILTER_VALIDATE_EMAIL);
    if ($email_validate){
        echo "Дякуємо за реєстрацію! Реєстрація пройшла успішно.";
    }else{
        echo "Помилка! Перевірте коректність введених даних";
    }
}
?>